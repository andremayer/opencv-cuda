import cv2
import numpy as np
import time


def grayVideo():
    
    print(cv2.getBuildInformation())
    
    cv2.ocl.setUseOpenCL(True)
    cv2.setUseOptimized(True)
    
    tempoComCuda = convertVideoComCuda(cv2.VideoCapture('turn-down-for-what.mp4'))
    tempoSemCuda = convertVideoSemCuda(cv2.VideoCapture('turn-down-for-what.mp4'))
    
    
    print("Tempo com CUDA:", tempoComCuda)
    print("Tempo sem CUDA:", tempoSemCuda)
    
    
def convertVideoComCuda(video):
    e1 = cv2.getTickCount()
    
    lastFrame = -1
    while video.isOpened():
        ret, frame = video.read()
        
        frameAtual = video.get(cv2.CAP_PROP_POS_FRAMES)
        if(frameAtual == lastFrame):
            break
        
        imgUMat = cv2.UMat(frame)
        gray = cv2.cvtColor(imgUMat, cv2.COLOR_BGR2GRAY)
        cv2.imshow("Com CUDA", gray)
    
        lastFrame = video.get(cv2.CAP_PROP_POS_FRAMES)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
        
       
    
    video.release()
    cv2.destroyAllWindows()
    e2 = cv2.getTickCount()
    total = (e2 - e1)/ cv2.getTickFrequency()
    return total
   
def convertVideoSemCuda(video): 
    e1 = cv2.getTickCount()
    lastFrame = -1
    while video.isOpened():
        ret, frame = video.read()
        
        frameAtual = video.get(cv2.CAP_PROP_POS_FRAMES)
        if(frameAtual == lastFrame):
            break
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('Sem CUDA', gray)
    
        lastFrame = video.get(cv2.CAP_PROP_POS_FRAMES)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    video.release()
    cv2.destroyAllWindows()
    e2 = cv2.getTickCount()
    total = (e2 - e1)/ cv2.getTickFrequency()
    return total

    
if __name__ == '__main__':
    grayVideo()